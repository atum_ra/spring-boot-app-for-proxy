package com.roman.spring.proxy.demo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import java.util.logging.Logger;
import java.io.IOException;
import com.jayway.jsonpath.*;

@RestController
@ConfigurationProperties(prefix = "dadata")
public class Controller {

	private final static Logger LOGGER = Logger.getLogger("PROXY-REQUEST");

	private String token;
    private String secret;

    /**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the secret
	 */
	public String getSecret() {
		return secret;
	}

	/**
	 * @param secret the secret to set
	 */
	public void setSecret(String secret) {
		this.secret = secret;
	}

	@RequestMapping("/")
	String home() {
		return "it works";
	}

	@RequestMapping("/proxy")
	public String greetingJson(WebRequest webRequest) {
		final String json = webRequest.getParameter("address");
		LOGGER.info("POST = " + json);

		String addressResourceUrl = "https://dadata.ru/api/v2/clean/address";
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new MyErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.add("Authorization", token);
		headers.add("X-Secret", secret);

		HttpEntity<String> request = new HttpEntity<String>(json, headers);

		try {
			ResponseEntity<String> response = restTemplate.postForEntity(
			addressResourceUrl, request , String.class);
			if (response.getStatusCodeValue() == 200) {
				String responseJson =  "данные недостоверные, измените запрос.";
				try {
					Object document = Configuration.defaultConfiguration().jsonProvider().parse(response.getBody());

					String geo_lat = JsonPath.read(document, "$[0].geo_lat");
					String geo_lon = JsonPath.read(document, "$[0].geo_lon");
					int qc = JsonPath.read(document, "$[0].qc");

					if ( qc == 0) {
						responseJson = "широта : "  + geo_lat + ", долгота : " + geo_lon;
					}
					if ( qc == 3) {
						responseJson =  "Есть альтернативные варианты. Уточните.";
					}

				} catch (Exception e) {
					LOGGER.severe("Error with JsonPath " + e.getMessage());
					return "{\"status\": 500}";
				}
				return responseJson;
			}
		} catch (Exception e) {
			LOGGER.severe("Error postForEntity  " + e.getMessage());
			return "{\"status\": 500}";
		}

		return "{\"status\": 500}";
	}

	public class MyErrorHandler implements ResponseErrorHandler {
		@Override
		public void handleError(ClientHttpResponse response) throws IOException {
			LOGGER.severe("Error with response from OpenData " + response.getStatusText());
			response.close();
		}

		@Override
		public boolean hasError(ClientHttpResponse response) throws IOException {
			HttpStatus statusCode = response.getStatusCode();
			return statusCode.series() == HttpStatus.Series.SERVER_ERROR;
		}
	  }
}

