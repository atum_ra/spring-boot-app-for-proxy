package com.roman.spring.proxy.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProxyMain {

	public static void main(String[] args) {
		SpringApplication.run(ProxyMain.class, args);
	}

}

